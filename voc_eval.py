from argparse import ArgumentParser

import mmcv
import numpy as np

from mmdet import datasets
from mmdet.core import eval_map


def voc_eval(result_file, dataset, iou_thr=0.6):

    confidences = np.arange(0, 1, 0.1)
    #confidences = [0.09]
    for conf in confidences:
        print(f'For confidence value {conf}')
        det_results = mmcv.load(result_file)
        gt_bboxes = []
        gt_labels = []
        gt_ignore = []
        annotated = []
        scores = np.array(det_results[0][0][:, 4])
        idxs = np.where(scores > conf)
        #aaa = det_results[0][0][:len(idxs[0]), :]
        for i in range(len(det_results)):
            det_results[i][0] = det_results[i][0][:len(idxs[0]), :]
        for i in range(len(dataset)):
            ann = dataset.get_ann_info(i)
            dict_= {}
            dict_['bboxes'] = ann['bboxes']
            dict_['labels'] = ann['labels']
            bboxes = ann['bboxes']
            labels = ann['labels']
            if 'bboxes_ignore' in ann:
                ignore = np.concatenate([
                    np.zeros(bboxes.shape[0], dtype=np.bool),
                    np.ones(ann['bboxes_ignore'].shape[0], dtype=np.bool)
                ])
                gt_ignore.append(ignore)
                bboxes = np.vstack([bboxes, ann['bboxes_ignore']])
            #labels = np.concatenate([labels, ann['labels_ignore']])
            gt_bboxes.append(bboxes)
            gt_labels.append(labels)
            annotated.append(dict_)
        if not gt_ignore:
            gt_ignore = None
        if hasattr(dataset, 'year') and dataset.year == 2007:
            dataset_name = 'voc07'
        else:
            dataset_name = dataset.CLASSES
        _, results = eval_map(
            det_results,
            annotated,
        #gt_bboxes,
        #gt_labels,
        #gt_ignore=gt_ignore,
        #scale_ranges=None,
            iou_thr=iou_thr,
        #dataset=dataset_name,
        #print_summary=True
        )
        nom = results[0]['precision'] * results[0]['recall']
        denom = results[0]['precision'] + results[0]['recall']
        #F1 = np.mean(2*(nom/denom))
        precision = np.mean(results[0]['precision'])
        recall = np.mean(results[0]['recall'])
        F1 = 2*((precision * recall) / (precision + recall))
        print(f'precision is : {precision}')
        print(f'recall is : {recall}')
        print(f'F1 is : {F1}')

        


def main():
    parser = ArgumentParser(description='VOC Evaluation')
    parser.add_argument('result', help='result file path')
    parser.add_argument('config', help='config file path')
    parser.add_argument(
        '--iou-thr',
        type=float,
        default=0.6,
        help='IoU threshold for evaluation')
    args = parser.parse_args()
    cfg = mmcv.Config.fromfile(args.config)
    test_dataset = mmcv.runner.obj_from_dict(cfg.data.test, datasets)
    voc_eval(args.result, test_dataset, args.iou_thr)


if __name__ == '__main__':
    main()